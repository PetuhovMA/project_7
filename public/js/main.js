$(document).ready(function() {
    console.log("DOM was loaded :)");
    $('.slider').slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive:[
            {
                breakpoint: 992,
                settings: {
                    slidesToShow:2,
                    slidesToScroll: 2
                }
            }
        ]
      });
});
